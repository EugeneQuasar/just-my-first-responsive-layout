let navButton = document.querySelector('.left-nav__burger')

let navMenu = document.querySelector('.header__left-nav')
let headerTopMenu = document.querySelector('.header__menu-items')
let socialBox = document.querySelector('.left-nav__social-box')
let addIco = document.querySelector('.burger_ico')

navButton.onclick = function () {
  navMenu.classList.toggle('left-nav__social-menu__display-block')
  headerTopMenu.classList.toggle('to-left-nav')
  socialBox.classList.toggle('left-nav__social-box-grid')
  addIco.classList.toggle('burger_ico-close')
}
